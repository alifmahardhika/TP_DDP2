//Alif Mahardhika; 1706021934; lastedited at 6/04/2018 19:50
//contributor: Fadhlan H.P., Roshani Ayu Pranasti
//import the utilities needed
import java.util.Scanner;
import java.util.ArrayList;
//import the packages needed
import cages.Cages;
import animals.*;

//creating Office class
public class Office {
    //creating array of animals
    private static ArrayList<Animals> listOfCat = new ArrayList<>();
    private static ArrayList<Animals> listOfLion = new ArrayList<>();
    private static ArrayList<Animals> listOfEagle = new ArrayList<>();
    private static ArrayList<Animals> listOfParrot = new ArrayList<>();
    private static ArrayList<Animals> listOfHamster = new ArrayList<>();

    //main method
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);    //creating a scanner for the inputs
        Office javariPark = new Office();          //instantiating an object of Office type
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        int[] number = new int[5];                 //creating an empty array to store the number of animals
        String[] animalTypes = {"cat", "lion", "eagle", "parrot", "hamster"};   //creating an array of animal types

         //looping to receive the number of animals from input
        for (int i = 0; i < 5; i++) {
            System.out.print(animalTypes[i] + ": ");
            int amount = input.nextInt();
            number[i] = amount;
            if (amount > 0) {
                javariPark.initiation(animalTypes[i]);      //calling the initiation method
            }
        }
        System.out.println("Animals have been successfully recorded!\n");
        System.out.println("=============================================");

        javariPark.printArrangement();              //printing the arrangement of cages

        System.out.println("NUMBER OF ANIMALS:");   //printing out the number of animals
        for (int k = 0; k < 5; k++){
            System.out.println(animalTypes[k] + ":" + number[k]);
        }
        System.out.println("\n=============================================");

        javariPark.visitAnimal();                   //calling the method to visit animals
    }

    //methods
    //initiation method
    private void initiation(String type) {
        System.out.println("Provide the information of " + type + "(s):");
        Scanner input = new Scanner(System.in);
        //recording the informations of animals
        String[] informations = input.nextLine().split(",");
        //inistantiating every animal within the list of animal
        for (String temp : informations) {
            String[] information = temp.split("\\|");   //splitting the character
            if (type.equals("cat")) {
                Cat cat = new Cat(information[0], Integer.parseInt(information[1]));
                listOfCat.add(cat);
            } else if (type.equals("lion")) {
                Lion lion = new Lion(information[0], Integer.parseInt(information[1]));
                listOfLion.add(lion);
            } else if (type.equals("eagle")) {
                Eagle eagle = new Eagle(information[0], Integer.parseInt(information[1]));
                listOfEagle.add(eagle);
            } else if (type.equals("parrot")) {
                Parrot parrot = new Parrot(information[0], Integer.parseInt(information[1]));
                listOfParrot.add(parrot);
            } else if (type.equals("hamster")) {
                Hamster hamster = new Hamster(information[0], Integer.parseInt(information[1]));
                listOfHamster.add(hamster);
            }
        }
    }
    //method to print the cage arrangements for each animal type
    private void printArrangement() {
        System.out.println("Cage arrangement:");
        if (listOfCat.size() > 0) {
            Cages catCage = new Cages(listOfCat);
            catCage.arrangeCages();
            catCage.rearrangeCages();
        }
        if (listOfLion.size() > 0) {
            Cages lionCage = new Cages(listOfLion);
            lionCage.arrangeCages();
            lionCage.rearrangeCages();
        }
        if (listOfEagle.size() > 0) {
            Cages eagleCage = new Cages(listOfEagle);
            eagleCage.arrangeCages();
            eagleCage.rearrangeCages();
        }
        if (listOfParrot.size() > 0) {
            Cages parrotCage = new Cages(listOfParrot);
            parrotCage.arrangeCages();
            parrotCage.rearrangeCages();
        }
        if (listOfHamster.size() > 0) {
            Cages hamsterCage = new Cages(listOfHamster);
            hamsterCage.arrangeCages();
            hamsterCage.rearrangeCages();
        }
    }
    //method to visit animal
    private void visitAnimal() {
        while (true) {
            System.out.print("Which animal do you want to visit?\n" +
                    "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: exit)\n");
            Scanner input = new Scanner(System.in);
            String option = input.nextLine();
            boolean availability = false;
            //choosing the animal options
            if (option.equals("1")) {
                //if no animal of the type is available
                if(listOfCat.size()==0){
                    System.out.println("There is no cat to visit");
                    System.out.println("Back to the office!\n");
                }
                else{
                    //if the animal type is available to visit
                    System.out.print("Mention the name of the cat you want to visit: ");
                    String tempName = input.nextLine();
                    for (Animals tempAnimal : listOfCat) {
                        if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                            System.out.println("You are visiting " + tempName +
                            " (cat) now, what would you like to do?\n" + "1: Brush the fur 2: Cuddle");
                            int tempNumber = Integer.parseInt(input.nextLine());
                            tempAnimal.action(tempNumber);
                            availability = true;
                        }
                    }
                    //if there is no animal with such name from the type
                    if (!availability) {
                        System.out.println("There is no cat with that name!");
                        System.out.println("Back to the office!\n");
                    }
                }
            } else if (option.equals("2")) {
                //if no animal of the type is available
                if(listOfEagle.size()==0){
                    System.out.println("There is no eagle to visit");
                    System.out.println("Back to the office!\n");
                }else{
                    //if the animal type is available to visit
                    System.out.print("Mention the name of the eagle you want to visit: ");
                    String tempName = input.nextLine();
                    for (Animals tempAnimal : listOfEagle) {
                        if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                            System.out.println("You are visiting " + tempName +
                            " (eagle) now, what would you like to do?\n" + "1: Order to fly");
                            int tempNumber = Integer.parseInt(input.nextLine());
                            tempAnimal.action(tempNumber);
                            availability = true;
                        }
                    }
                    //if there is no animal with such name from the type
                    if (!availability) {
                        System.out.println("There is no eagle with that name!");
                        System.out.println("Back to the office!\n");
                    }
                }
            } else if (option.equals("3")) {
                //if no animal of the type is available
                if(listOfHamster.size()==0){
                    System.out.println("There is no hamster to visit");
                    System.out.println("Back to the office!\n");
                }else{
                    //if the animal type is available to visit
                    System.out.print("Mention the name of the hamster you want to visit: ");
                    String tempName = input.nextLine();
                    for (Animals tempAnimal : listOfHamster) {
                        if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                            System.out.println("You are visiting " + tempName + " (hamster) now, what would you like to do?\n" +
                                    "1: See it gnawing 2: Order to run in the hamster wheel");
                            int tempNumber = Integer.parseInt(input.nextLine());
                            tempAnimal.action(tempNumber);
                            availability = true;
                        }
                    }
                    //if there is no animal with such name from the type
                    if (!availability) {
                        System.out.println("There is no hamster with that name!");
                        System.out.println("Back to the office!\n");
                    }
                }
            } else if (option.equals("4")) {
                //if no animal of the type is available
                if(listOfParrot.size()==0){
                    System.out.println("There is no parrot to visit");
                    System.out.println("Back to the office!\n");
                    } else {
                    //if the animal type is available to visit
                    System.out.print("Mention the name of the parrot you want to visit: ");
                    String tempName = input.nextLine();
                    for (Animals tempAnimal : listOfParrot) {
                        if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                            System.out.println("You are visiting " + tempName + " (parrot) now, what would you like to do?\n" +
                                    "1: Order to fly 2: Do conversation");
                            int tempNumber = Integer.parseInt(input.nextLine());
                            tempAnimal.action(tempNumber);
                            availability = true;
                        }
                    }
                    //if there is no animal with such name from the type
                    if (!availability) {
                        System.out.println("There is no parrot with that name!");
                        System.out.println("Back to the office!\n");
                    }
                }
            } else if (option.equals("5")) {
                //if no animal of the type is available
                if(listOfLion.size()==0){
                    System.out.println("There is no lion to visit");
                    System.out.println("Back to the office!\n");
                } else {
                    //if the animal type is available to visit
                    System.out.print("Mention the name of the lion you want to visit: ");
                    String tempName = input.nextLine();
                    for (Animals tempAnimal : listOfLion) {
                        if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                            System.out.println("You are visiting " + tempName + " (lion) now, what would you like to do?\n" +
                                    "1: See it hunting 2: Brush the mane 3: Disturb it");
                            int tempNumber = Integer.parseInt(input.nextLine());
                            tempAnimal.action(tempNumber);
                            availability = true;
                        }
                    }
                    //if there is no animal with such name from the type
                    if (!availability) {
                        System.out.println("There is no lion with that name!");
                        System.out.println("Back to the office!\n");
                    }
                }
            } else {
                System.out.println("Choose a valid option");
                break;
            }
        }
    }
}
