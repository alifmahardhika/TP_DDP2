package cages;
import java.util.ArrayList;
import animals.Animals;

public class Cages {

    //Instance Variable of Cages
    private ArrayList<Animals> listBefore;
    private Animals listAfter[][];
    private int size;

    //Constructor of Cages
    public Cages(ArrayList<Animals> listBefore) {
        this.listBefore = listBefore;
    }
    //arrange the cages
    public void arrangeCages() {
        int indeks = 0;
        //if the number of cagees made is more than three
        if (listBefore.size() > ((listBefore.size() / 3) * 3)) {
            size = listBefore.size() / 3;
            indeks = 3 - (listBefore.size() - size * 3);
        //if the number of cages is less than three
        } else {
            size = listBefore.size() / 3 - 1;
        }
        //creating a new list for the arranged cage
        listAfter = new Animals[3][size + 1];
        int indeks2 = 0;

        if (size == 0) {            //if there is no cages made
            for (int a = 0; a < listBefore.size(); a++) {
                listAfter[a][0] = listBefore.get(a);
            }
        } else {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < size + 1; j++) {
                    if (j == size && i < indeks) {
                        break;
                    } else {
                        listAfter[i][j] = listBefore.get(indeks2);
                        indeks2++;
                    }
                }
            }
        }
        //specify the location of cage
        System.out.println("location: " + listAfter[0][0].getLocation());
        printAnimal(listAfter, size);
        System.out.println();
    }

    public void rearrangeCages() {          //reversing the arrangement of cages
        Animals listAfterArrange[][];       //creating a new list for the reversed arrangement
        listAfterArrange = new Animals[3][size + 1];
        int temp1 = 1;
        for (int i = 2; i >= 0; i--) {
            int temp2 = size;
            if (temp1 == -1) {
                temp1 = 2;
            }
            for (int j = 0; j < size + 1; j++) {
                listAfterArrange[i][j] = listAfter[temp1][temp2];
                temp2--;
            }
            temp1--;
        }
        System.out.println("After rearrangement...");
        printAnimal(listAfterArrange, size);            //printing the final arranged cage
        System.out.println();
    }

    //method to print the animals inside the cage
    private void printAnimal(Animals listAnimal[][], int size) {
        for (int k = 2; k >= 0; k--) {
            System.out.print("Level " + (k + 1) + ": ");
            for (int l = 0; l < size + 1; l++) {
                if (listAnimal[k][l] != null) {
                    System.out.print(listAnimal[k][l].getName() + "(" +
                            listAnimal[k][l].getLength() + " - " +
                            listAnimal[k][l].getCageType() + "), ");
                }
            }
            System.out.print("\n");
        }
    }
}
