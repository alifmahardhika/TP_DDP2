package animals;        //declaring package name
import java.util.Scanner;       //importing scanner to receive inputs

public class Parrot extends Animals {

	//constructor
    public Parrot(String name, int length) {
        super(name, length, false);
    }

    //methods
    public void action(int number) {
        if (number == 1) {
            this.ordertoFly();
        } else if (number == 2) {
            this.doConversation();
        } else {
            System.out.format("%s says: HM?", this.getName());
        }

        System.out.println("Back to the office!\n");
    }

    private void ordertoFly() {
        System.out.format("Parrot %s flies!%n", this.getName());
		System.out.format("%s makes a voice: FLYYYY.....%n", this.getName());
    }

    private void doConversation() {
        Scanner input = new Scanner(System.in);
        System.out.print("You say: ");
        String command = input.nextLine();      //receiving words to be resaid by the parrot
        System.out.format("%s says: %s%n", this.getName(), command.toUpperCase());
    }
}
