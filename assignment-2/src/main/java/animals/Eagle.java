package animals;    //declaring package name

public class Eagle extends Animals {

	//constructor
  	public Eagle(String name, int length) {
        super(name, length, true);
    }

    //methods
    public void action(int number) {
        if (number == 1) {
            this.orderToFly();
        } else {
            System.out.println("You do nothing...");
        }

        System.out.println("Back to the office!\n");
    }

    private void orderToFly() {
        System.out.format("%s makes a voice: kwaakk...&n", this.getName());
        System.out.println("You hurt!");
    }
}
