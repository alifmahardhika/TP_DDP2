package animals;    //declaring package name
//importing the utilities needed
import java.util.Arrays;
import java.util.Random;

public class Cat extends Animals {

    //constructor
    public Cat(String name, int length) {
        super(name, length, false);
    }

    //methods
    public void action(int number) {
        if (number == 1) {
            this.brushTheFur();
        } else if (number == 2) {
            this.cuddle();
        } else {
            System.out.println("You do nothing...");
        }

        System.out.println("Back to the office!\n");
    }
    private void brushTheFur() {
        System.out.format("Time to clean %s's fur%n", this.getName());
        System.out.format("%s makes a voice: Nyaaan...%n", this.getName());
    }
    private void cuddle() {
    	String[] catVoices = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"}; //creating array of cat voices
    	Random randomize = new Random();       //to randomize number. used to choose what sound is created
        System.out.format("%s makes a voice: %s%n", this.getName(), catVoices[randomize.nextInt(4)]);
    }
}
