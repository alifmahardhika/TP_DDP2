package animals;    //declaring package name

public class Hamster extends Animals {

	//constructor
    public Hamster(String name, int length) {
        super(name, length, false);
    }

    //methods
    public void action(int number) {
        if (number == 1) {
            this.seeItGnawing();
        } else if (number == 2) {
            this.orderToRun();
        } else {
            System.out.println("You do nothing...");
        }

        System.out.println("Back to the office!\n");
    }

    private void seeItGnawing() {
       System.out.format("%s makes a voice: ngkkrit.. ngkkrrriiit%n", this.getName());
    }

    private void orderToRun() {
        System.out.format("%s makes a voice: trrr.... trrr...%n", this.getName());
    }
}
