package animals;    //declaring package name

public class Animals {

    //instance Variable of Animals
    protected String name, location;
    protected int length, lengthOfCage, widthOfCage;
    protected boolean isWild;
    protected char cageType;

    //constructor of Animals
    public Animals(String name, int length, boolean isWild) {
        this.name = name;
        this.length = length;
        this.isWild = isWild;
        if (isWild == false) {
            //if the animal location is indoor
        	this.location = "indoor";
            //specify the cage type
			if (length < 45) {
				this.lengthOfCage = 60;
				this.widthOfCage = 60;
				this.cageType = 'A';
			} else if (length <= 60) {
				this.lengthOfCage = 60;
				this.widthOfCage = 90;
				this.cageType = 'B';
			} else {
				this.lengthOfCage = 60;
				this.widthOfCage = 120;
				this.cageType = 'C';
			}

		} else {
            //if the animal location is outdoor
			this.location = "outdoor";
            //specify the cage type
			if (length < 75) {
				this.lengthOfCage = 120;
				this.widthOfCage = 120;
				this.cageType = 'A';
			} else if (length <= 90) {
				this.lengthOfCage = 120;
				this.widthOfCage = 150;
				this.cageType = 'B';
			} else {
				this.lengthOfCage = 120;
				this.widthOfCage = 180;
				this.cageType = 'C';
			}
		}
    }

    //getter & setter
    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }

    public String getLocation() {
        return location;
    }

    public boolean getIsWild() {
        return isWild;
    }

    public char getCageType() {
        return cageType;
    }

    //creating an overridable
    public void action(int number) {
    }
}
