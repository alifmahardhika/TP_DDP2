//importing libraries needed
import javax.swing.*;

public class Button extends JButton {

 	private ImageIcon image;  //instance variables for assigned images
 	private Button self;
    public static ImageIcon cover = new ImageIcon("memes/master.png"); //cover images

    /*
    *constructor for Button type object
    */
	public Button(ImageIcon image, int x, int y, JLabel label) {
		super(image);
		this.image = image; //showing assigned image
		setBounds(x,y,100,100); //setting the button's dimension
	}

    /**
    * method to set the value of buttons
    * @param Button to set and a label
    * adds an action listener
    */
	public void setButton(Button self, JLabel label) {
		this.self = self;
    	addActionListener(new ButtonAction(self, label));
	}
    /**
    * method to get assigned image
    * @return assigned image
    */
	public ImageIcon getImage() {
    	return image;
	}
    /**
    * method to get cover image
    * @return cover image
    */
    public ImageIcon getCover() {
        return cover;
    }
}
