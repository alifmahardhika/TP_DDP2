//Alif Mahardhika; 1706021934
//Contributors: Roshani Ayu Pranasti, Grahana Daffa Herlambang, M. Indra R.

//importing libraries needed
import javax.swing.*;
import java.awt.event.*;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class MemeMemoryGame {
    //attributes
	private static ArrayList<ImageIcon> imageList = new ArrayList<ImageIcon>();
    private JFrame frame = new JFrame("Meme Memory Game");  //created here so it can be called from anywhere in the code
    private ArrayList<Button> cardList = new ArrayList<Button>();

    //constructor
	public  MemeMemoryGame() {
		loadImage();
		generateGUI();
	}
    /**
    * method to clear the image list, used when restarting a Game
    */
    public  static void clearImageList(){
        imageList.clear();
    }
    /**
    * resizing image to feasible dimension to show
    * @param ImageIcon
    * @return resized ImageIcon
    */
    public static ImageIcon resize(ImageIcon icon) {
        Image image = icon.getImage();
        Image sizeNow = image.getScaledInstance(100,100, Image.SCALE_SMOOTH);
        return new ImageIcon(sizeNow);
    }
    /**
    * method to load the images from the specified file folder
    * only accepts specific files with specific name length
    * @param none
    * @return none
    */
	public static void loadImage() {
        try{
            File folder = new File("memes");
    		File[] listOfFiles = folder.listFiles();
    		for (File file : listOfFiles) {
    			if ((file.getName().length() == 12)) {
                    ImageIcon temp = new ImageIcon(file.getAbsolutePath());
    				imageList.add(resize(temp)); //calling the method resize
    			}
    		}
            //adding previous image doubles for the pairs
    		for (int i = 0; i < 18; i++) {
    			imageList.add(imageList.get(i));
    		}
        } catch (Exception e) {
            System.out.println(e.getClass());
        }

	}
    /**
    * method to randomize image list
    * @param ArrayList<ImageIcon>
    */
	public static void randomizeImage(ArrayList<ImageIcon> listToRandom) {
        //to randomize only from the current/local stream of numbers
		Random random = ThreadLocalRandom.current();
        //randomize the index of image list
    	for (int i = listToRandom.size() - 1; i > 0; i--) {
      		int index = random.nextInt(i + 1);
      		ImageIcon temp = listToRandom.get(index);
      		listToRandom.set(index, listToRandom.get(i));
      		listToRandom.set(i, temp);
    	}
	}
    /**
    * generating the whole GUI frame
    */
  	public void generateGUI() {
    JLabel label = new JLabel("Number of Attempt(s): 0");
	int x = 3, y = 3, index = 0; //used to set the dimension and position of buttons
    ArrayList<ImageIcon> copyList = new ArrayList<ImageIcon>();
    // making a copy of Image list to randomize, as it is the best practice to do
    for (ImageIcon temp : imageList) {
        copyList.add(temp);
    }
    randomizeImage(copyList); //randomize the copyList
        /**
        *this block creates buttons (six to six dimension),
        * with assigned image from the randomized list
        */
		for (int i = 1; i <= 6; i++) {
			for (int j = 1; j <= 6; j++) {
				Button button = new Button(copyList.get(index),x,y,label);
				button.setButton(button,label);
                cardList.add(button); //adding created buttonto list of cards
				frame.add(button);
				y += 100;
				index++;
			}
		    y = 3; //once six horizontally button is created, it resets its y value
	        x += 100; //once six horizontally button is created, the x value moved 100 pixels lower
	   }
       //creating exit button
       JButton exitButton = new JButton("Exit Game");
       exitButton.setBounds(400, 605, 100, 24);
       exitButton.addActionListener(actions -> exitGame());
       frame.add(exitButton);

       //creating replay button
       JButton replayButton = new JButton("Play Again");
       replayButton.setBounds(500, 605, 100, 24);
       replayButton.addActionListener(actions -> restartButton());
       frame.add(replayButton);

       //creating button to show all cards
       JButton showButton = new JButton("Show all");
       showButton.setBounds(195, 605, 105, 24);
       showButton.addActionListener(actions -> showAll());
       frame.add(showButton);

       //creating button to close all cards
       JButton closeCard = new JButton("Close all");
       closeCard.setBounds(300, 605, 100, 24);
       closeCard.addActionListener(actions -> closeAll());
       frame.add(closeCard);

       //setting the "Number of attempts" label
       label.setFont((new Font("Gadget", Font.PLAIN, 16)));
       label.setBounds(0, 605, 199, 24);
       frame.add(label);

       //setting the game frame
       frame.setSize(620,670);
       frame.setLayout(null);
       frame.setVisible(true);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 	}
    /**
    * method for restarting the game, called when "play Again" button is pressed
    * creates a new option pane to confirm user's action
    */
    public  void restartButton(){
        Object[] choice = {"New Game", "Cancel"}; //options
        int option = JOptionPane.showOptionDialog(null, "  " +
        "                    Start a new game?", "Restart Game",
            JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, choice, choice[1]);
        if (option == 0) { //if the "Play Again" button in this frame is selected
            frame.dispose(); //dispose previous frame
            clearImageList(); //clearing image list
            new MemeMemoryGame(); //starts a new game

        }
    }
    public void showAll(){
        Object[] choice = {"I AM A CHEATER", "Cancel"}; //options
        int option = JOptionPane.showOptionDialog(null, "  " +
        "     Are you sure? Your'e about to cheat.", "Show Card",
            JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, choice, choice[1]);
        if (option == 0) { //if the "Show cards" button in this frame is selected
            for (Button temp:cardList) {
                temp.setIcon(temp.getImage());
            }
        }
    }
    public void closeAll(){
        for (Button temp:cardList) {
            temp.setIcon(temp.getCover());
        }

    }
    /**
    * method for restarting the game, called when "Exit Game" button is pressed
    * creates a new option pane to confirm user's action
    */
    public void exitGame(){
        Object[] choice = {"Exit", "Cancel"};
        int option = JOptionPane.showOptionDialog(null, "    Are you sure? Your'e about to exit", "Exit Game",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, choice, choice[1]);
        if (option == 0) {
            frame.dispose();
        }
    }
    /**
    * main method
    */
	public static void main(String[] args) {
		MemeMemoryGame game = new MemeMemoryGame();
	}
}
