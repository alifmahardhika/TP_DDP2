//importing libraries needed
import javax.swing.*;
import java.awt.event.*;

public class ButtonAction implements ActionListener {
    //attributes
    public static ImageIcon cover = setCoverImage();
    public static boolean inDelay = false;  //setting some time delay so card won't close too quick
    public static int attempt = 0; //attempt counts
    public static Button prev = null;   //previous button chosen
    private Button self;
    private JLabel label;

    //constructor
    public ButtonAction(Button self, JLabel label) {
        this.self = self;
        this.label = label;
        self.setIcon(cover);
    }
    /**
    * called when a pair of cards matches
    * @return an ActionListener
    */
    private static ImageIcon setCoverImage(){
        try { //trying the following action
            return new ImageIcon("memes/master.png");
        } catch(Exception e) { //if file is not found, catch the error, print a message
            System.out.println("Cover Image File Not Found");
            return null;
        }
    }
  	private ActionListener match = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
            self.setEnabled(false);
      		prev.setEnabled(false);
      		prev = null;
            inDelay = false;
        }
  	};
    /**
    * called when a pair of cards does not match
    * @return an ActionListener
    */
  	private ActionListener notMatch = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
      		self.setIcon(cover);
      		prev.setIcon(cover);
      		prev = null;
            inDelay = false;
        }
  	};
    /**
    * Override performed action
    */
  	@Override
  	public void actionPerformed(ActionEvent e) {
        /*
        * runs when event is in delay
        * do nothing while in delay
        */
        if (inDelay) {
            return;
        }
        /*
        * when delay time is passed
        * checks if card pair is matched or not
        */
        if (prev == null) {
          	prev = self;
          	self.setIcon(self.getImage());
        } else if (prev.getImage() == self.getImage() && self != prev) {
            //the above is so if a card is selected twice it is not considered a match
            label.setText("Number of Attempt(s): " + ++attempt);
            self.setIcon(self.getImage()); //setting button icon with it's assigned image
            inDelay = true; //starting delay
            //setting delay time for 0.5 second, then calls the method match
            Timer timer = new Timer(500, match);
            timer.setRepeats(false); //no repeats
            timer.start(); //starting timer
        } else {
            if (prev != self){  //for not matching pairs
                label.setText("Number of Attempt(s): " + ++attempt);
              	self.setIcon(self.getImage()); //showing image, same as above
                inDelay = true;
              	Timer timer = new Timer(500, notMatch);
              	timer.setRepeats(false);
              	timer.start();
            }

        }
    }
}
