//ALIF MAHARDHIKA 1706021934; LAST EDITED 12/03/2018 (12.20);
import java.util.Scanner;
import java.util.LinkedList;

public class A1Station {

    //CONSTRUCTOR
    private static final double THRESHOLD = 250; // In kilograms
    private static LinkedList <TrainCar> track = new LinkedList <TrainCar>();
    private static TrainCar recent;
    private static double weight = 0;

    //MAIN METHOD
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);         //RECEIVING INPUT

        int numberOfCats = input.nextInt();             //NUMBER OF CATS
        if (numberOfCats == 0){
        	return;
        } else {
            for (int i = 0; i < numberOfCats; i++) {
            	String[] described = input.next().split(",");
            	if (!track.isEmpty()) {
            		recent = track.getFirst();
            		track.addFirst(new TrainCar(new WildCat(described[0], Double.parseDouble(described[1]), Double.parseDouble(described[2])), recent));
            		weight = track.getFirst().computeTotalWeight();
            		if (weight >= THRESHOLD) {
            			go();
        			}
        		} else {
            		track.addFirst(new TrainCar(new WildCat(described[0], Double.parseDouble(described[1]), Double.parseDouble(described[2]))));
            		weight = track.getFirst().computeTotalWeight();
            		if (weight >= THRESHOLD) {
            			go();
            		}
            	}
            }
        }

        if (!track.isEmpty()) {
            go(); // When there is no further input and there is a train on the track, depart the train immediately.
        }
    }

    //CATEGORIZING THE CATS BY ITS BMI
    public static void go() {
    	double averageMassIndex = track.getFirst().computeTotalMassIndex() / track.size();
    	String category = "";
    	if (averageMassIndex <= 18.5) {
    		category += "underweight";
    	} else if (averageMassIndex >= 18.5 && averageMassIndex < 25) {
    		category += "normal";
    	} else if (averageMassIndex >= 25 && averageMassIndex < 30) {
    		category += "overweight";
    	} else if (averageMassIndex >= 30) {
    		category += "obese";
    	}
        //OUTPUTS
    	System.out.println("The train departs to Javari Park");
    	System.out.print("[LOCO]<--");
    	track.getFirst().printCar();
    	System.out.printf("Average mass index of all cats: %.2f\n", averageMassIndex);
    	System.out.format("In average, the cats in the train are *%s*\n",category);
    	track.clear();
    }
}
