public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // CONSTRUCTOR AND ATTRIBUTES
    public WildCat cat;
    public TrainCar next;

    // constructor overloading
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    // methods
    public double computeTotalWeight() {            //TOTAL WEIGHT
        if (next == null) {
            return cat.weight + EMPTY_WEIGHT;
        } else {
            return cat.weight + EMPTY_WEIGHT + next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {         //BODY MASS INDEX
        if (next == null) {
            return cat.computeMassIndex();
        } else {
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        //OUTPUTS
        if (next == null) {
            System.out.format("(%s)\n", cat.name);
        } else {
            System.out.format("(%s) --", cat.name);
            next.printCar();
        }
    }
}
