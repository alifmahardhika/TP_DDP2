public class WildCat {

    //ATTRIBUTES &CONSTRUCTORS
    public String name;
    public double weight; // In kilograms
    public double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    // method
    public double computeMassIndex() {
    	double bmi = (weight /((length * 0.01)*(length * 0.01)));
        return bmi;
    }
}
