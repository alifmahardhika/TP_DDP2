package javari.park;

import java.util.ArrayList;
import java.util.Arrays;

public class CountingMasters extends Attractions{
    
    private static String[] validAnimals = {"Hamster", "Whale", "Parrot"};
    private ArrayList<String> listOfAnimals = new ArrayList<String>();

    public CountingMasters(String name, String type){
        super(name, type);
    }

    public static boolean checkValidity(String type){
        for(int y = 0; y < validAnimals.length; y++){
            if(type.equals(validAnimals[y])){
                return true;
            }
        }

        return false;
    }
}