package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitors implements Registration {

    private int id;
    private String name;
    private List<SelectedAttraction> attractions = new ArrayList<SelectedAttraction>();

    public Visitors(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getRegistrationId() {
        return id;
    }

    public String getVisitorName() {
        return name;
    }

    public String setVisitorName(String name) {
        String oldName = this.name;
        this.name = name;
        return oldName + " changed to" + name;
    }

    public List<SelectedAttraction> getSelectedAttractions() {
        return attractions;
    }

    public boolean addSelectedAttraction(SelectedAttraction selected) {
        if (selected==null) {
            return false;
        } else {
            attractions.add(selected);
            return true;
        }
    }
}
