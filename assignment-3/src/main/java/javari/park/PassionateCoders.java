package javari.park;

import java.util.ArrayList;
import java.util.Arrays;

public class PassionateCoders extends Attractions{
    
    private static String[] validAnimals = {"Hamster", "Cat", "Snake"};
    private ArrayList<String> listOfAnimals = new ArrayList<String>();

    public PassionateCoders(String name, String type){
        super(name, type);
    }

    public static boolean checkValidity(String type){
        for(int y = 0; y < validAnimals.length; y++){
            if(type.equals(validAnimals[y])){
                return true;
            }
        }

        return false;
    }

}