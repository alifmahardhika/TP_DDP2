package javari.reader;

import java.io.IOException;
import java.nio.file.Path;

public class ReaderAnimalRecords extends CsvReader {

    public ReaderAnimalRecords(Path File) throws IOException {
        super(File);
    }

    public long countValidRecords() {
        long valid = 0;
        for(String line : lines) {
            String[] data = line.split(COMMA);
            if (data.length == 8) {
                valid += 1;
            }
        }
        return valid;
    }

    public long countInvalidRecords() {
        long invalid = 0;
        for (String line : lines) {
            String[] data = line.split(COMMA);
            if (data.length != 8) {
                invalid+=1;
            }
        }
        return invalid;
    }
}