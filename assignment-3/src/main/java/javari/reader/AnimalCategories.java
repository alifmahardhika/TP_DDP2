package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javari.animal.*;
import javari.park.*;
import java.util.Set;
import java.util.LinkedHashSet;

public class AnimalCategories extends CsvReader{
    
    public AnimalCategories(Path file) throws IOException{
        super(file);
    }

    public ArrayList<String> category = new ArrayList<String>();

    public long countValidRecords(){
        Set<String> valid = new LinkedHashSet<>();
        for(int x = 0; x < lines.size(); x++){
            String baris = lines.get(x);
            String[] barisSplit = baris.split(",");
            switch(barisSplit[1]){
                case "mammals":
                    if(Arrays.asList(Attractions.getValidCategories()).contains(barisSplit[2]) && Arrays.asList(Mammals.getMamalia()).contains(barisSplit[0])){
                        valid.add(barisSplit[2]);
                    }
                case "aves":
                    if(Arrays.asList(Attractions.getValidCategories()).contains(barisSplit[2]) && Arrays.asList(Aves.getAves()).contains(barisSplit[0])){
                        valid.add(barisSplit[2]);
                    }
                case "reptiles":
                    if(Arrays.asList(Attractions.getValidCategories()).contains(barisSplit[2]) && Arrays.asList(Reptiles.getReptilia()).contains(barisSplit[0])){
                        valid.add(barisSplit[2]);
                    }
            }

        }
        return (long)valid.size();
    }

    public long countInvalidRecords(){
        long invalid = 0;
        for(int x = 0; x < lines.size(); x++){
            String baris = lines.get(x);
            String[] barisSplit = baris.split(",");
            switch(barisSplit[1]){
                case "mammals":
                    if(!(Arrays.asList(Attractions.getValidCategories()).contains(barisSplit[2])) && (!(Arrays.asList(Mammals.getMamalia()).contains(barisSplit[0])))){
                        invalid += 1;
                    }
                case "aves":
                    if(!(Arrays.asList(Attractions.getValidCategories()).contains(barisSplit[2])) && (!(Arrays.asList(Aves.getAves()).contains(barisSplit[0])))){
                        invalid += 1;
                    }
                case "reptiles":
                    if(!(Arrays.asList(Attractions.getValidCategories()).contains(barisSplit[2])) && (!(Arrays.asList(Reptiles.getReptilia()).contains(barisSplit[0])))){
                        invalid += 1;
                    }
            }

        }
        return invalid;
    }

}